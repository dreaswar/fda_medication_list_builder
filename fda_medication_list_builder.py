#! /usr/bin/python

import json, StringIO

__all_fields__ = ['ApplNo',
                  'ProductNo',
                  'Form',
                  'Dosage',
                  'ProductMktStatus',
                  'TECode',
                  'ReferenceDrug',
                  'drugname',
                  'activeingred'
                  ]

json_list = []

initial_data     = file('initial_data.json','w')
fda_product_list = open('Product.txt','r')

pk = 0
for line in fda_product_list.readlines():
  medication_list_dict={}
  medication_list_dict['fields']={
                                  'drug_name'        : line.split('\t')[7].strip('\t\n\r'),
                                  'active_ingredient': line.split('\t')[8].strip('\t\n\r'),
                                  'dosage'           : line.split('\t')[3].strip('\t\n\r'),
                                  'form'             : line.split('\t')[2].strip('\t\n\r')
                                  }
  medication_list_dict['model'] = 'drug_db.FDADrugs'
  medication_list_dict['pk'] = pk
  
  #appl_no = line.split('\t')[0]
  #product_no = line.split('\t')[1]
  #form = line.split('\t')[2]
  #dosage = line.split('\t')[3]
  #product_mkt_status = line.split('\t')[4]
  #te_code = line.split('\t')[5]
  #reference_drug = line.split('\t')[6]
  #drug_name = line.split('\t')[7]
  #active_ingredient = line.split('\t')[8]

  json_list.append(medication_list_dict)
  print line.split('\t')[0]
  pk +=1

initial_json_data = json.dumps(json_list, sort_keys=False, indent=4)
initial_data.write(initial_json_data)
initial_data.close()